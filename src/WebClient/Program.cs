﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        private static readonly HttpClient client = new HttpClient();
        static async Task Main(string[] args)
        {
            await CustomerGetRequest();
            await RandomCustomer();
        }

        private static async Task RandomCustomer()
        {
            Random rnd = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Console.WriteLine("Adding new user...");
            Customer customer = new Customer()
            {
                Id = rnd.Next(1, 100),
                Firstname = new string(Enumerable.Repeat(chars, 10).Select(s => s[rnd.Next(s.Length)]).ToArray()),
                Lastname = new string(Enumerable.Repeat(chars, 10).Select(s => s[rnd.Next(s.Length)]).ToArray())
            };
            string json = JsonSerializer.Serialize(customer);
            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var postTask = await client.PostAsync("https://localhost:5001/customers", stringContent);
            if(postTask.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine(postTask.StatusCode);
                int id = int.Parse(await postTask.Content.ReadAsStringAsync());
                var getTask = client.GetStringAsync($"https://localhost:5001/customers/{id}");
                var msg = await getTask;
                Console.WriteLine(msg);
            } else
            {
                Console.WriteLine("Error\n");
                Console.WriteLine(postTask.StatusCode);
            }
           
        }

        private static async Task CustomerGetRequest()
        {
            Console.WriteLine("Enter user id: ");
            string id = Console.ReadLine();
            var getTask = client.GetStringAsync($"https://localhost:5001/customers/{id}");
            var msg = await getTask;
            Console.WriteLine(msg);
        }
    }
}