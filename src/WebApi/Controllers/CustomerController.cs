using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private IRepository _repository;

        public CustomerController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id:long}")]   
        public IActionResult GetCustomerAsync([FromRoute] long id)
        {
            var user = _repository.customers.SingleOrDefault(e => e.Id == id);
            if(user != null)
            {
                 return Ok(user);
            } else
            {
                return NotFound();
            }
            
        }

        [HttpPost("")]   
        public IActionResult CreateCustomerAsync([FromBody] Customer customer)
        {
            try
            {
                _repository.Add(customer);
                return Ok(customer.Id);
            }
            catch { 
                return Conflict(); 
            }
            
        }
    }
}