﻿using System.Linq;

namespace WebApi.Models
{
    public class CustomerRepository: IRepository
    {
        private ApplicationDbContext _context;
        public CustomerRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IQueryable<Customer> customers => _context.Customers; 
        public int Add(Customer customer)
        {
            _context.Customers.Add(customer);
            _context.SaveChanges();
            return customer.Id;
        }
    }
}
