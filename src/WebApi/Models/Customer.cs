﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApi.Models
{
    public partial class Customer
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}
