﻿using System.Linq;

namespace WebApi.Models
{
    public interface IRepository
    {
        IQueryable<Customer> customers { get; }
        int Add(Customer customer);
    }
}
